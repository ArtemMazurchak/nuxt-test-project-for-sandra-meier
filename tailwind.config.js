module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      'Lato' : ['Lato Medium']
    },
    colors: {
      'theme-green': '#319795',
      'link-text-color': '#E6FFFA',
      'h-grey': '#2D3748',
      'tab-h-grey': '#4A5568',
      'border-gray': '#CBD5E0',
      'active-tab': '#81E6D9',
      'bg-list-index': '#F7FAFC',
      'list-item-index': '#718096'
    },
    boxShadow: {
      btm: '0px 3px 6px #00000029',
      tp: '0px -3px 6px #00000029',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('tailwindcss-pixel-dimensions')
  ],
}
